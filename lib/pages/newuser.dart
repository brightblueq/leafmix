import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

//import '../widgets/ui_elements/adapative_progress_indicator.dart';
import '../scoped-models/main.dart';
import '../models/auth.dart';

import '../pages/login.dart';

class NewUserPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NewUserPage();
  }
}

class _NewUserPage extends State<NewUserPage> with TickerProviderStateMixin {
  final Map<String, dynamic> _formData = {
    'email': null,
    'password': null,
    'acceptTerms': false
  };
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _passwordTextController = TextEditingController();
  AuthMode _authMode = AuthMode.Login;
  AnimationController _controller;
  Animation<Offset> _slideAnimation;

  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );
    _slideAnimation =
        Tween<Offset>(begin: Offset(0.0, -1.5), end: Offset.zero).animate(
      CurvedAnimation(parent: _controller, curve: Curves.fastOutSlowIn),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.80;

//    String nameValue = '';
//    String emailValue = '';
//    bool acceptTerms = false;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lime,
        title: Text("NEW USER"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            width: targetWidth,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 75.0,
                ),
                SizedBox(
                  height: 10.0,
                ),
                _buildEmailTextField(),
                SizedBox(
                  height: 10.0,
                ),
                _buildPasswordTextField(),
                SizedBox(
                  height: 10.0,
                ),
                _buildPasswordConfirmTextField(),
                SizedBox(
                  height: 75.0,
                ),
                Container(
                    width: 250,
                    child: Text(
                      'TERMS OF SERVICER',
                      style: TextStyle(fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    )),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                    width: 250,
                    child: Text(
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Suspendisse in est ante in nibh mauris cursus mattis molestie. Eu augue ut lectus arcu bibendum at varius vel.",
                      textAlign: TextAlign.justify,
                      style: TextStyle(),
                    )),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                      alignment: Alignment.center,
                      width: 250,
                      child: Text(
                        'Click HERE for more details.',
                        textAlign: TextAlign.justify,
                        style: TextStyle(),
                      )),
                ),
                SizedBox(
                  height: 50.0,
                ),
                RaisedButton(
                  textColor: Colors.white,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  },
                  color: Colors.lime,
                  child: Text('ACCEPT'),
                ),
              ],
            ),
          ),
        ),
      ),

//      body: Container(
////        decoration: BoxDecoration(
////          image: _buildBackgroundImage(),
////        ),
////        padding: EdgeInsets.all(10.0),
//        child: Center(
//          child: SingleChildScrollView(
//            child: Container(
//              width: targetWidth,
//              child: Form(
//                key: _formKey,
//                child: Column(
//                  children: <Widget>[
////                    SizedBox(
////                      height: 250.0,
////                    ),
//                    _buildEmailTextField(),
//                    SizedBox(
//                      height: 10.0,
//                    ),
//                    _buildPasswordTextField(),
//                    SizedBox(
//                      height: 10.0,
//                    ),
//                    _buildPasswordConfirmTextField(),
////                    _buildAcceptSwitch(),
//                    SizedBox(
//                      height: 10.0,
//                    ),
////                    FlatButton(
////                      child: Text(
////                          'Switch to ${_authMode == AuthMode.Login ? 'Signup' : 'Login'}'),
////                      onPressed: () {
//////                        if (_authMode == AuthMode.Login) {
//////                          setState(() {
//////                            _authMode = AuthMode.Signup;
//////                          });
//////                          _controller.forward();
//////                        } else {
//////                          setState(() {
//////                            _authMode = AuthMode.Login;
//////                          });
//////                          _controller.reverse();
//////                        }
////                      },
////                    ),
//                    SizedBox(
//                      height: 10.0,
//                    ),
//                  RaisedButton(
//                    textColor: Colors.white,
//                    child: Text('SIGNUP'),
////                          child: Text(_authMode == AuthMode.Login
////                              ? 'LOGIN'
////                              : 'SIGNUP'),
//                    onPressed: () => {
//
//                    }
////                        _submitForm(model.authenticate),
//                  ),
////                    ScopedModelDescendant<MainModel>(
////                      builder: (BuildContext context, Widget child,
////                          MainModel model) {
//////                        return model.isLoading
//////                            ? AdaptiveProgressIndicator()
//////                            : RaisedButton(
////                          RaisedButton(
////                          textColor: Colors.white,
////                          child: Text('SIGNUP'),
//////                          child: Text(_authMode == AuthMode.Login
//////                              ? 'LOGIN'
//////                              : 'SIGNUP'),
////                          onPressed: () =>
////                              _submitForm(model.authenticate),
////                        );
////                      },
////                    ),
//                  ],
//                ),
//              ),
//            ),
//          ),
//        ),
//      ),
    );
  }

  void _submitForm(Function authenticate) async {
    if (!_formKey.currentState.validate() || !_formData['acceptTerms']) {
      return;
    }
    _formKey.currentState.save();
    Map<String, dynamic> successInformation;
    successInformation = await authenticate(
        _formData['email'], _formData['password'], _authMode);
    if (successInformation['success']) {
      // Navigator.pushReplacementNamed(context, '/');
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('An Error Occurred!'),
            content: Text(successInformation['message']),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

//  DecorationImage _buildBackgroundImage() {
//    return DecorationImage(
//      fit: BoxFit.scaleDown,
//      alignment: Alignment.topCenter,
//      // colorFilter:
//      //     ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
//      image: AssetImage('assets/leafmix_logo.png'),
//    );
//  }

  Widget _buildEmailTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'E-Mail', filled: true, fillColor: Colors.white),
      keyboardType: TextInputType.emailAddress,
      validator: (String value) {
        if (value.isEmpty ||
            !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                .hasMatch(value)) {
          return 'Please enter a valid email';
        }
      },
      onSaved: (String value) {
        _formData['email'] = value;
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Password', filled: true, fillColor: Colors.white),
      obscureText: true,
      validator: (String value) {
        if (value.isEmpty || value.length < 6) {
          return 'Password invalid';
        }
      },
      onSaved: (String value) {
        _formData['password'] = value;
      },
    );
  }

  Widget _buildPasswordConfirmTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Confirm Password', filled: true, fillColor: Colors.white),
      obscureText: true,
      controller: _passwordTextController,
      validator: (String value) {
        if (_passwordTextController.text != value &&
            _authMode == AuthMode.Signup) {
          return 'Passwords do not match.';
        }
      },
    );
  }

  Widget _buildAcceptSwitch() {
    return SwitchListTile(
      value: _formData['acceptTerms'],
      onChanged: (bool value) {
        setState(() {
          _formData['acceptTerms'] = value;
        });
      },
      title: Text('Accept Terms'),
    );
  }
}
