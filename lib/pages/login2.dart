import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../pages/auth.dart';
import '../pages/newuser.dart';
import '../pages/product_list.dart';
import '../pages/products.dart';
import '../pages/stream.dart';

import '../widgets/ui_elements/adapative_progress_indicator.dart';
import '../scoped-models/main.dart';
import '../models/auth.dart';

class Login2Page extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Login2PageState();
  }
}

class _Login2PageState extends State<Login2Page> with TickerProviderStateMixin {
  final Map<String, dynamic> _formData = {
    'email': null,
    'password': null,
    'acceptTerms': false
  };
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _passwordTextController = TextEditingController();
  AuthMode _authMode = AuthMode.Login;

//  AnimationController _controller;
//  Animation<Offset> _slideAnimation;

  void initState() {
//    _controller = AnimationController(
//      vsync: this,
//      duration: Duration(milliseconds: 300),
//    );
//    _slideAnimation =
//        Tween<Offset>(begin: Offset(0.0, -1.5), end: Offset.zero).animate(
//      CurvedAnimation(parent: _controller, curve: Curves.fastOutSlowIn),
//    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 100, bottom: 25),
              child: Container(
                height: 250,
                width: 250,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/leafmix_logo.png'),
                  ),
                ),
              ),
            ),
//            SizedBox(
//              height: 10.0,
//            ),
            SizedBox(
              height: 20.0,
            ),
            Padding(
              padding: EdgeInsets.all(2.0),
              child: Text(
                'Select Login',
                style: TextStyle(
                  color: Colors.black45,
                ),
              ),
            ),
            Row(
              children: <Widget>[
                _buildButton('EMAIL', Colors.green, 40, 40),
              ],
            ),
//            SizedBox(
//              height: 10.0,
//            ),
            Row(
              children: <Widget>[
                _buildButton('FACEBOOK', Colors.blue, 40, 5),
                _buildButton('GOOGLE', Colors.red, 5, 40),
              ],
            ),
            Row(
              children: <Widget>[
                _buildButton('TWITTER', Colors.lightBlueAccent, 40, 5),
                _buildButton('INSTAGRAM', Colors.amber, 5, 40),
              ],
            ),
            SizedBox(
              height: 65.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AuthPage()),
                    );
                  },
                  color: Colors.lightGreen[300],
                  child: Text('CANCEL'),
                ),
              ],
            ),
            SizedBox(
              height: 65.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding:
                  EdgeInsets.only(left: 0, right: 2, top: 0, bottom: 0),
                  child: Text('New to LeafMix?', textAlign: TextAlign.right),
                ),
//                ),
                Padding(
                  padding:
                  EdgeInsets.only(left: 2, right: 0, top: 0, bottom: 0),
                  child: InkWell(
                    child: Text(
                      'Join Us!',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: Colors.green,
                          fontWeight: FontWeight.w500),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewUserPage()),
                      );
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

//  void _submitForm(Function authenticate) async {
//    if (!_formKey.currentState.validate() || !_formData['acceptTerms']) {
//      return;
//    }
//    _formKey.currentState.save();
//    Map<String, dynamic> successInformation;
//    successInformation = await authenticate(
//        _formData['email'], _formData['password'], _authMode);
//    if (successInformation['success']) {
//      // Navigator.pushReplacementNamed(context, '/');
//    } else {
//      showDialog(
//        context: context,
//        builder: (BuildContext context) {
//          return AlertDialog(
//            title: Text('An Error Occurred!'),
//            content: Text(successInformation['message']),
//            actions: <Widget>[
//              FlatButton(
//                child: Text('Okay'),
//                onPressed: () {
//                  Navigator.of(context).pop();
//                },
//              )
//            ],
//          );
//        },
//      );
//    }


//  Widget _buildEmailTextField() {
//    return TextFormField(
//      decoration: InputDecoration(
//          labelText: 'E-Mail', filled: true, fillColor: Colors.white),
//      keyboardType: TextInputType.emailAddress,
//      validator: (String value) {
//        if (value.isEmpty ||
//            !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
//                .hasMatch(value)) {
//          return 'Please enter a valid email';
//        }
//      },
//      onSaved: (String value) {
//        _formData['email'] = value;
//      },
//    );
//  }
//
//  Widget _buildPasswordTextField() {
//    return TextFormField(
//      decoration: InputDecoration(
//          labelText: 'Password', filled: true, fillColor: Colors.white),
//      obscureText: true,
//      controller: _passwordTextController,
//      validator: (String value) {
//        if (value.isEmpty || value.length < 6) {
//          return 'Password invalid';
//        }
//      },
//      onSaved: (String value) {
//        _formData['password'] = value;
//      },
//    );
//  }
//
//  Widget _buildPasswordConfirmTextField() {
//    return FadeTransition(
//      opacity: CurvedAnimation(parent: _controller, curve: Curves.easeIn),
//      child: SlideTransition(
//        position: _slideAnimation,
//        child: TextFormField(
//          decoration: InputDecoration(
//              labelText: 'Confirm Password',
//              filled: true,
//              fillColor: Colors.white),
//          obscureText: true,
//          validator: (String value) {
//            if (_passwordTextController.text != value &&
//                _authMode == AuthMode.Signup) {
//              return 'Passwords do not match.';
//            }
//          },
//        ),
//      ),
//    );
//  }

  Widget _buildButton(String _name, Color _color, double _left, double _right) {
    return Container(
      child: Expanded(
          child: Padding(
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AuthPage()),
                  );
                },
                color: _color,
//                child: Text('$_name'),
                child: Icon(Icons.ac_unit),

              ),
              padding: EdgeInsets.only(
                  left: _left, right: _right, top: 0, bottom: 0))),
    );
  }


//  DecorationImage _buildBackgroundImage() {
//    return DecorationImage(
//      fit: BoxFit.scaleDown,
//      alignment: Alignment.topCenter,
//      image: AssetImage('assets/leafmix_logo.png'),
//    );
//  }
}
