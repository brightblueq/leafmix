import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../pages/login.dart';

import '../widgets/products/products.dart';
import '../widgets/ui_elements/logout_list_tile.dart';
import '../scoped-models/main.dart';

class StreamPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StreamPageState();
  }
}

class _StreamPageState extends State<StreamPage> {
  @override
  initState() {
//    widget.model.fetchProducts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[100],
      appBar: AppBar(
//          title: Image.asset('assets/leafmix_logo.png', fit: BoxFit.cover),
        title: Title(
          color: Colors.white,
          child: Text(
            'LeafMix',
            style: TextStyle(
              fontFamily: 'Tahoma',
              fontWeight: FontWeight.bold,
              fontSize: 30,
            ),
          ),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.green,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              child: Icon(
                Icons.album,
                color: Colors.white,
              ),
              color: Colors.green[700],
              onPressed: () {},
            ),
            RaisedButton(
              child: Icon(
                Icons.bookmark,
                color: Colors.white,
              ),
              color: Colors.green[700],
              onPressed: () {},
            ),
            RaisedButton(
              child: Icon(
                Icons.cloud_circle,
                color: Colors.white,
              ),
              color: Colors.green[700],
              onPressed: () {},
            ),
            RaisedButton(
              child: Icon(
                Icons.account_circle,
                color: Colors.white,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
//  Widget build(BuildContext context) {
//    return ScopedModelDescendant<MainModel>(
//      builder: (BuildContext context, Widget child, MainModel model) {
//        return ListView.builder(
//          itemBuilder: (BuildContext context, int index) {
//            return Dismissible(
//              key: Key(model.allProducts[index].title),
//              onDismissed: (DismissDirection direction) {
//                if (direction == DismissDirection.endToStart) {
//                  model.selectProduct(model.allProducts[index].id);
//                  model.deleteProduct();
//                } else if (direction == DismissDirection.startToEnd) {
//                  print('Swiped start to end');
//                } else {
//                  print('Other swiping');
//                }
//              },
//              background: Container(color: Colors.red),
//              child: Column(
//                children: <Widget>[
//                  ListTile(
//                    leading: CircleAvatar(
//                      backgroundImage:
//                          NetworkImage(model.allProducts[index].image),
//                    ),
//                    title: Text(model.allProducts[index].title),
//                    subtitle:
//                        Text('\$${model.allProducts[index].price.toString()}'),
//                    trailing: _buildEditButton(context, index, model),
//                  ),
//                  Divider()
//                ],
//              ),
//            );
//          },
//          itemCount: model.allProducts.length,
//        );
//      },
//    );
//  }
//}

Widget _buildStream() {
  return ScopedModelDescendant(
    builder: (BuildContext context, Widget child, MainModel model) {
      Widget content = Center(child: Text('No Products Found!'));
      if (model.displayedProducts.length > 0 && !model.isLoading) {
        content = Products();
      } else if (model.isLoading) {
        content = Center(child: CircularProgressIndicator());
      }
      return RefreshIndicator(
        onRefresh: model.fetchProducts,
        child: content,
      );
    },
  );
}

Widget _buildEditButton(BuildContext context, int index, MainModel model) {
  return IconButton(
    icon: Icon(Icons.edit),
    onPressed: () {
//      model.selectProduct(model.allProducts[index].id);
//      Navigator.of(context).push(
//        MaterialPageRoute(
//          builder: (BuildContext context) {
//            return ProductEditPage();
//          },
//        ),
//      ).then((_) {
//        model.selectProduct(null);
//      });
    },
  );
}
