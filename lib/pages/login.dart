import 'package:flutter/material.dart';

import 'package:scoped_model/scoped_model.dart';

import '../pages/auth.dart';
import '../pages/newuser.dart';
import '../pages/stream.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[100],
      appBar: AppBar(
//          title: Image.asset('assets/leafmix_logo.png', fit: BoxFit.cover),
        title: Title(
          color: Colors.white,
          child: Text(
            'Sign In',
            style: TextStyle(
              fontFamily: 'Tahoma',
              fontWeight: FontWeight.normal,
              fontSize: 22,
            ),
          ),
        ),
//        automaticallyImplyLeading: false,
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 75, bottom: 25),
              child: Container(
                height: 125,
                width: 125,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/leafmix_logo.png'),
                  ),
                ),
              ),
            ),
//            SizedBox(
//              height: 10.0,
//            ),
            SizedBox(
              height: 20.0,
            ),
            Padding(
              padding: EdgeInsets.all(2.0),
              child: Text(
                'Select Login',
                style: TextStyle(
                  color: Colors.black54,
                ),
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AuthPage()),
                        );
                      },
                      color: Colors.green,
                      child: Icon(
                        Icons.email,
                        color: Colors.white,
                      ),
                    ),
                    padding: EdgeInsets.only(left: 80, right: 80),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AuthPage()),
                        );
                      },
                      color: Colors.blue,
                      child: Icon(
                        Icons.face,
                        color: Colors.white,
                      ),
                    ),
                    padding: EdgeInsets.only(left: 80, right: 5),
                  ),
                ),
                Expanded(
                  child: Padding(
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AuthPage()),
                        );
                      },
                      color: Colors.red,
                      child: Icon(
                        Icons.account_circle,
                        color: Colors.white,
                      ),
                    ),
                    padding: EdgeInsets.only(left: 5, right: 80),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
//                  child: Padding(
                  child: Padding(
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AuthPage()),
                        );
                      },
                      color: Colors.lightBlueAccent,
                      child: Icon(
                        Icons.mobile_screen_share,
                        color: Colors.white,
                      ),
                    ),
                    padding: EdgeInsets.only(left: 80, right: 5),
                  ),
                ),
                Expanded(
                  child: Padding(
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AuthPage()),
                        );
                      },
                      color: Colors.amber,
                      child: Icon(
                        Icons.ac_unit,
                        color: Colors.white,
                      ),
                    ),
                    padding: EdgeInsets.only(left: 5, right: 80),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 75.0,
            ),
            Padding(
              padding: EdgeInsets.only(left: 0, right: 2, top: 0, bottom: 0),
              child: Text(
                'New to LeafMix?',
                style: TextStyle(
                  color: Colors.black54,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => NewUserPage()),
                        );
                      },
                      color: Colors.green[700],
                      child: Icon(
                        Icons.add_box,
                        color: Colors.white,
                      ),
                    ),
                    padding: EdgeInsets.only(left: 120, right: 120),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
//                ),
//                Padding(
//                  padding:
//                      EdgeInsets.only(left: 2, right: 0, top: 0, bottom: 0),
//                  child: InkWell(
//                    child: Text(
//                      'Join Us!',
//                      textAlign: TextAlign.left,
//                      style: TextStyle(
//                          decoration: TextDecoration.underline,
//                          color: Colors.green,
//                          fontWeight: FontWeight.w500),
//                    ),
//                    onTap: () {
//                      Navigator.push(
//                        context,
//                        MaterialPageRoute(builder: (context) => NewUserPage()),
//                      );
//                    },
//                  ),
//                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.green,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              SizedBox(
                height: 40.0,
              ),
            ]),
      ),
    );
  }
}
