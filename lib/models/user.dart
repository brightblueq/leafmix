import 'package:flutter/material.dart';

class User {
  final String id;
  final String email;
  final String token;
  final int seeds;
  final int donations;

  User({@required this.id, @required this.email, @required this.token, this.seeds, this.donations });
}
